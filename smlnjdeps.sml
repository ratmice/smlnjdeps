structure SmlnjDeps = struct
  type Option = string * string option

(*  The result from the command line parsing will
be a list of file names and a set of options. *)
  type CmdLine = (Option list) * (string list)

(*  This exception will bomb with a usage message. *)
  exception Usage of string

  fun parse_cmdline argv : CmdLine =
  let
    fun loop [] opts = (opts, [])
      | loop ("-h"::rest) opts = loop rest (("help", NONE) :: opts)
      | loop ("-v"::rest) opts = loop rest (("verbose", NONE) :: opts)
      | loop ("-verbose"::rest) opts = loop rest (("verbose", NONE) :: opts)
      | loop ("-arch"::rest)  opts = get_value "arch"  rest opts
      | loop ("-os"::rest)  opts = get_value "os"  rest opts
      | loop ("-D"::rest) opts = get_value "D" rest opts
      | loop (arg::rest) opts =
          (if String.sub(arg, 0) = #"-"
    	    then raise Usage (concat["The option ", arg, " is unrecognised."])
  	    else (opts, arg::rest) (* the final result *))
    and get_value name [] opts = (raise Usage (concat["The value for the option ", name, " is missing."]))
      | get_value name (v::rest) opts = (loop rest ((name, SOME v) :: opts))
  in loop argv [] end

  fun find_option opts name : (string option) option
	 = (case List.find (fn (n, v) => n = name) opts
	     of NONE => NONE
	      | SOME (n, v) => SOME v)

  and has_option opts name = (find_option opts name) <> NONE
  and require_option opts name and_value : string =
	(case find_option opts name
	  of NONE => raise Usage (concat["The option ’", name, "’ is missing."])
	   | SOME NONE =>  (* found but has no value *)
		(if and_value
		  then raise Usage (concat["The option ’", name, "’ is missing a value."])
		  else "")
	   | SOME (SOME v) => v  (* found and has a value *))

  fun display x = (print x; print "\n");
  fun testCM (x:{file:string, derived:bool, class:string}, found:bool) =
    if (not o #derived) x andalso (#class x <> "cm")
     then (display (#file x); true) 
     else found;
  fun dumpCM (SOME(sources), found) = List.foldr testCM found sources
    | dumpCM (NONE, found) = found 
  fun dump oa files =
          List.foldr (fn (x, y) => dumpCM ((CM.sources oa x), y)) false files;
  fun main (prog, args) =
    let 
	val (opts, files) =  parse_cmdline args
	val os = require_option opts "os" true
	val arch = require_option opts "arch" true
	val os_arch=SOME({os=os, arch=arch})
	val _ = #set CM.Control.verbose false;
	val _ = #set CM.Control.debug false;
	val _ = #set CM.Control.warn_obsolete false;
	val foundSomething = dump os_arch files;
	fun exit (true) = OS.Process.success
	  | exit (false) = OS.Process.failure
    in exit (foundSomething)
   end
  handle Usage msg => 
    (TextIO.output(TextIO.stdErr,
		   concat[msg, "\nUsage: [-h] [-v|-verbose] [-os name]"," [-arch name] cmfile\n"]);
     OS.Process.failure)
end

structure Main = SmlnjDeps;
