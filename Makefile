
include smlnjvars.mk
BIN=bin
PROJECT:=smlnjdeps
smlnjdeps:=$(shell pwd)/smlnjdeps
CM_FILES:=test smlnjdeps

.PHONY: check smlnj
.PRECIOUS: %.$(HEAP_SUFFIX)

smlnj: $(PROJECT)

clean:
	rm -f $(PROJECT) $(PROJECT).$(HEAP_SUFFIX) test test.$(HEAP_SUFFIX)

test: smlnjdeps

check: test
	make test
	make test
	touch test.sml
	make test

install: $(PROJECT)
	mkdir -p $(DESTDIR)/$(BIN)
	install -t $(DESTDIR)/$(BIN) $(PROJECT).$(HEAP_SUFFIX)
	(echo "#!/bin/sh";echo "sml @SMLload=$(DESTDIR)/$(BIN)/$(PROJECT).$(HEAP_SUFFIX) \$$@") >$(DESTDIR)/$(BIN)/$(PROJECT)
	chmod +x $(DESTDIR)/$(BIN)/$(PROJECT)
        

# This works by generating a rule something like: 
# file: file1.sml file2.sml 
#
# where the $(shell ...) output newlines are conveted into spaces
# So, don't try naming your files #foo.sml which will comment out all of the dependencies.
#
# to debug s/eval/warning.
$(foreach file,\
   $(CM_FILES),\
   $(eval $(file).$(HEAP_SUFFIX):$(shell $(smlnjdeps) -os $(OS) -arch $(ARCH) $(file).cm)))


# Assume Main.main.
%.$(HEAP_SUFFIX): %.cm
	(echo 'CM.make "$<";';echo 'SMLofNJ.exportFn("$@",Main.main);') | CM_VERBOSE=false sml

%: %.$(HEAP_SUFFIX)
	(echo "#!/bin/sh";echo "sml @SMLload=$< \$$@") >$@
	chmod +x $@
