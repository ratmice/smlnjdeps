smlnjdeps is a tool for generating makefile depdendencies
from the SML/NJ CM build system

it is an alternative to the ml-makedepend tool which comes with it,
as I have some issues with the above.

Installing with smackage:
smackage source smlnjdeps git https://gitlab.com/ratmice/smlnjdeps.git
smackage refresh
smackage get smlnjdeps
smackage make smlnjdeps smlnj

It should then show up in your $SMACKAGE_PATH/bin

There is an example project using it available:
https://gitlab.com/ratmice/sml-build-template

